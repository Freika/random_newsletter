require 'rails_helper'

feature 'link' do
  describe 'create' do
    let(:newsletter)       { create(:newsletter, :with_user) }
    let(:link_url)         { FFaker::Internet.http_url }
    let(:link_title)       { FFaker::Book.name }
    let(:link_description) { FFaker::Book.description }

    before do
      sign_in newsletter.user
    end

    it 'should create valid link and set it to not suggested' do
      click_on 'Newsletters'
      click_on  newsletter.title
      fill_in  'link_url',         with: :link_url
      fill_in  'link_title',       with: :link_title
      fill_in  'link_description', with: :link_description
      click_on 'Offer link'
      expect(Link.find_by(url: :link_url)).not_to be_suggested
    end
  end
end
