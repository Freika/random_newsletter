require 'rails_helper'

feature 'authentication' do
  describe 'user sign up' do
    let(:email) { FFaker::Internet.free_email }
    let(:password) { FFaker::Internet.password }

    it 'should create user and newsletter' do
      expect {
        visit root_path
        click_on 'Registration'
        fill_in 'user_email', with: email
        fill_in 'user_password', with: password
        fill_in 'user_password_confirmation', with: password
        click_on 'Sign Up'
      }.to change(User, :count).by(1)

      newsletter = User.find_by(email: email).newsletter

      expect(newsletter.title).to eq("#{newsletter.user[:email]}'s newsletter")
    end
  end

  describe 'user sign in' do
    let(:user) { create :user }

    scenario 'valid user' do
      visit root_path
      click_on 'Login'
      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: user.password
      click_on 'Sign In'

      expect(page).to have_content('Signed in successfully.')
    end

    scenario 'invalid user' do
      visit root_path
      click_on 'Login'
      fill_in 'user_email', with: 'bad_email@example.com'
      fill_in 'user_password', with: 'bad_password'
      click_on 'Sign In'

      expect(page).to have_content('Invalid Email or password.')
    end
  end

  describe 'update user profile', js: true do
    let(:user) { create :user }

    before do
      sign_in user
    end

    scenario 'with valid params' do
      visit root_path
      click_on user.email
      click_on 'Your settings'
      fill_in 'user_password', with: 'new_password'
      fill_in 'user_password_confirmation', with: 'new_password'
      fill_in 'user_current_password', with: user.password
      click_on 'Update Profile'

      expect(page).to have_content('Your account has been updated successfully.')
    end
  end
end
