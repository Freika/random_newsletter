require 'rails_helper'

feature 'send issue' do
  include ActiveJob::TestHelper

  describe 'sending' do
    let(:issue)        { create(:issue, :with_newsletter) }
    let(:sended_issue) { create(:issue, :with_newsletter, sent_at: Time.current) }

    context 'new issue' do
      it 'should send issue and show success flash message' do
        sign_in issue.newsletter.user
        expect {
          click_on 'View your newsletter'
          click_on issue[:title]
          click_on 'Send issue'
        }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
        expect(page).to have_content("#{issue[:title]} was succesfully sended.")
      end
    end

    context 'sended issue' do
      it 'should not show Send Issue button and show sent time' do
        sign_in sended_issue.newsletter.user
        click_on 'View your newsletter'
        click_on sended_issue[:title]
        expect(page).to have_content(sended_issue[:sent_at])
        expect(page).not_to have_content('Send issue')
      end
    end
  end
end
