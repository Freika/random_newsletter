require 'rails_helper'

feature 'issue' do
  describe 'create', js: true do
    let(:newsletter) { create(:newsletter, :with_user) }
    let(:issue_title) { FFaker::Book.name }
    let(:issue_description) { FFaker::Book.description }

    before do
      sign_in newsletter.user
    end

    it 'should be valid and has valid number' do
      expect {
        click_on 'New issue'
        fill_in 'issue_content', with: :issue_description
        click_on 'Save'
      }.to change(Issue, :count).by(1)
      expect(Issue.last[:number]).to eql(1)
    end
  end

  describe 'create list' do
    let(:newsletter) { create(:newsletter, :with_user) }

    it 'should be valid and has valid number' do
      2.times do
        issue = build(:issue, :with_slug, newsletter: newsletter)
        issue.set_number
        issue.save!
      end

      expect(newsletter.issues.first[:number]).to eql(1)
      expect(newsletter.issues.last[:number]).to eql(2)
    end
  end
end
