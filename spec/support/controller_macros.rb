module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      newsletter = FactoryGirl.create(:newsletter, :with_user)
      sign_in newsletter.user
    end
  end
end
