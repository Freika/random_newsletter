RSpec.configure do |config|
  config.before :suite do
    DatabaseCleaner[:active_record, connection: :test].clean_with :truncation
    DatabaseCleaner[:active_record, connection: :mail_database_test].clean_with :truncation
  end

  config.before do
    DatabaseCleaner[:active_record, connection: :mail_database_test].strategy = :truncation
    DatabaseCleaner.start
  end
end
