require 'rails_helper'

describe Newsletter, type: :model do
  it { belong_to(:user) }
  it { have_many(:issues) }
  it { have_many(:subscribers) }
end
