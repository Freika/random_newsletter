require 'rails_helper'

RSpec.describe User, type: :model do
  it { have_one(:newsletter) }
  it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }
  it { should validate_presence_of(:password) }

  it 'has a valid factory' do
    expect(FactoryGirl.build(:user)).to be_valid
  end
end
