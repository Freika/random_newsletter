require 'rails_helper'

describe Link, type: :model do
  it { belong_to(:newsletter) }
  it { should validate_presence_of(:url) }

  let(:link_1) { create :link }
  let(:link_2) { create :link }

  it 'clears query from link' do
    link_2.mark_repeat_domain!
    expect(link_2.repeat_domain).to be_truthy
  end
end
