require 'rails_helper'

RSpec.describe Subscriber, type: :model do
  it { belong_to(:newsletter) }
  it { should validate_presence_of(:name) }
end
