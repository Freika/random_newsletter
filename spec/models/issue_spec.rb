require 'rails_helper'

describe Issue, type: :model do
  describe 'validations' do
    let(:newsletter) { create(:newsletter, :with_user) }
    let(:issue) { create(:issue, newsletter: newsletter) }

    subject { issue }

    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:content) }
    it { belong_to(:newsletter) }
  end

  it 'have generated title' do
    issue = build(:issue, :with_newsletter)
    issue.set_title

    expect(issue.title).to eq(
      "#{issue.newsletter.title} — Issue ##{issue.number}"
    )
  end

  it 'have parameterized title as slug' do
    issue = create(:issue, :with_newsletter)

    expect(issue.slug).to include(issue.newsletter.title.parameterize)
  end
end
