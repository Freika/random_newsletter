require "rails_helper"

RSpec.describe NewsletterMailer, type: :mailer do
  describe 'new subscriber' do
    let(:subscriber) { create(:subscriber, :with_newsletter) }
    let(:mail) { NewsletterMailer.new_subscriber(subscriber).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('You have a new subscriber!')
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([subscriber.newsletter.user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['random@example.com'])
    end
  end
end
