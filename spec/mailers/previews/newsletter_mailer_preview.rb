# Preview all emails at http://localhost:3000/rails/mailers/newsletter_mailer
class NewsletterMailerPreview < ActionMailer::Preview

  def new_subscribe
     NewsletterMailer.new_subscriber(Subscriber.first)
  end
end
