require 'rails_helper'

RSpec.describe App::IssuesController, type: :controller do
  login_user

  let(:newsletter_attributes) { attributes_for(:issue, :with_newsletter) }
  let(:valid_attributes) { attributes_for(:issue, :with_slug) }
  let(:invalid_attributes) { {title: 'Invalid issue'} }

  describe "GET #show" do
    it "returns a success response" do
      issue = Issue.create! newsletter_attributes
      get :show, params: {id: issue.to_param}
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      issue = Issue.create! newsletter_attributes
      get :edit, params: {id: issue.to_param}
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Issue" do
        expect {
          post :create, params: {issue: valid_attributes}
        }.to change(Issue, :count).by(1)
      end

      it "redirects to the created issue" do
        post :create, params: {issue: valid_attributes}
        expect(response).to redirect_to([:app, Issue.last])
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {issue: invalid_attributes}
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) { {content: 'Updated'} }

      it "updates the requested issue" do
        issue = Issue.create! newsletter_attributes
        put :update, params: {id: issue.to_param, issue: new_attributes}
        issue.reload
        expect(issue.content).to eq('Updated')
      end

      it "redirects to the issue" do
        issue = Issue.create! newsletter_attributes
        put :update, params: {id: issue.to_param, issue: new_attributes}
        expect(response).to redirect_to([:app, issue])
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        issue = Issue.create! newsletter_attributes
        put :update, params: {id: issue.to_param, issue: invalid_attributes}
        expect(response.status).to eq(302)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested issue" do
      issue = Issue.create! newsletter_attributes
      expect {
        delete :destroy, params: {id: issue.to_param}
      }.to change(Issue, :count).by(-1)
    end

    it "redirects to the issues list" do
      issue = Issue.create! newsletter_attributes
      delete :destroy, params: {id: issue.to_param}
      expect(response).to redirect_to(app_issues_url)
    end
  end

end
