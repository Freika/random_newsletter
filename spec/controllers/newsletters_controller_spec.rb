require 'rails_helper'

RSpec.describe NewslettersController, type: :controller do

  describe 'GET #show' do
    let(:newsletter) { create(:newsletter, :with_user) }

    it 'returns http success' do
      get :show, params: { id: newsletter.slug }
      expect(response).to have_http_status(:success)
    end
  end

end
