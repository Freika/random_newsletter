require 'rails_helper'

RSpec.describe App::NewslettersController, type: :controller do
  describe 'authenticated user' do
    login_user

    describe "GET #edit" do
      it "returns http success" do
        get :edit
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #show" do
      it "returns http success" do
        get :show
        expect(response).to have_http_status(:success)
      end
    end

    describe "GET #update" do
      let(:valid_attributes) { attributes_for(:newsletter, :with_user) }

      it "returns http success" do
        put :update, params: { newsletter: valid_attributes }

        expect(response).to have_http_status(302)
      end
    end
  end
end
