require 'rails_helper'

RSpec.describe IssuesController, type: :controller do
  let(:newsletter) { create(:newsletter, :with_user) }
  let(:issue) { create(:issue, :with_slug, newsletter: newsletter) }

  describe 'GET #show' do
    it 'returns http success' do
      get :show, params: { newsletter_id: newsletter.slug, id: issue.slug }
      expect(response).to have_http_status(:success)
    end
  end
end
