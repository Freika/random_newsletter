require 'rails_helper'

RSpec.describe LinksController, type: :controller do
  describe 'POST #create' do
    let(:valid_link_attributes)   { attributes_for(:link, :with_newsletter) }
    let(:invalid_link_attributes) { { url: '' } }
    let(:newsletter_1)            { create(:newsletter, :with_user) }

    it 'should redirect to newsletter' do
      post :create, params:
        { link: valid_link_attributes, newsletter_id: newsletter_1.id }
      expect(response).to redirect_to newsletter_1
    end

    context "with invalid params" do
      it "should redirect to newsletter" do
        post :create, params:
          { link: invalid_link_attributes, newsletter_id: newsletter_1.id }
        expect(response).to redirect_to newsletter_1
      end
    end
  end
end
