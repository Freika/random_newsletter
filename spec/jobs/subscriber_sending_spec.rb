require 'rails_helper'

RSpec.describe SubscriberSendingJob, type: :job do
  include ActiveJob::TestHelper

  let(:issue)       { create(:issue, :with_newsletter) }
  let(:subscriber)  { create(:subscriber, :with_newsletter) }
  subject(:job)     { described_class.perform_later(subscriber[:id], issue[:id]) }
  
  it 'queues the job' do
    expect {
      job
    }.to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
  end

  it 'is in default queue' do
    expect(SubscriberSendingJob.new.queue_name).to eq('default')
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
