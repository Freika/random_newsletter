FactoryGirl.define do
  factory :subscriber do
    name { FFaker::Name.name }
    email { FFaker::Internet.email }

    trait :with_newsletter do
      newsletter { create :newsletter, :with_user }
    end
  end
end
