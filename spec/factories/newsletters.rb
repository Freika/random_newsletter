FactoryGirl.define do
  factory :newsletter do
    title { FFaker::Book.title }
    description { FFaker::Book.description }
    welcome_text { FFaker::Lorem.paragraph }
    slug { generate_slug }

    trait :with_user do
      user { create :user }
    end
  end
end
