FactoryGirl.define do
  factory :link do
    url         { FFaker::Internet.http_url }
    title       { FFaker::Book.title }
    description { FFaker::Book.description }

    trait :with_newsletter do
      newsletter { create :newsletter, :with_user }
    end
  end
end
