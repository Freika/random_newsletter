FactoryGirl.define do
  factory :issue do
    title { FFaker::Book.title }
    content { FFaker::Book.description }

    trait :with_slug do
      slug { generate_slug }
    end

    trait :with_newsletter do
      slug { generate_slug }
      newsletter { create :newsletter, :with_user }
    end
  end
end
