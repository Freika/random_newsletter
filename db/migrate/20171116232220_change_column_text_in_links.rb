class ChangeColumnTextInLinks < ActiveRecord::Migration[5.1]
  def change
    change_column :links, :url, :text, null: false
  end
end
