class AddSendAtToIssues < ActiveRecord::Migration[5.1]
  def change
    add_column :issues, :send_at, :datetime
  end
end
