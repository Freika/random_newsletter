class AddNewsletterIdToIssue < ActiveRecord::Migration[5.1]
  def change
    add_column :issues, :newsletter_id, :integer
    add_index :issues, :newsletter_id
  end
end
