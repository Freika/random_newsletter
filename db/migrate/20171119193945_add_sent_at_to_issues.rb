class AddSentAtToIssues < ActiveRecord::Migration[5.1]
  def change
    add_column :issues, :sent_at, :datetime
  end
end
