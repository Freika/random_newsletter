class CreateNewsletters < ActiveRecord::Migration[5.1]
  def change
    create_table :newsletters do |t|
      t.string :title
      t.text :description
      t.integer :user_id
      t.text :welcome_text

      t.timestamps
    end
    add_index :newsletters, :user_id
  end
end
