class AddIssueNumberToIssues < ActiveRecord::Migration[5.1]
  def change
    add_column :issues, :number, :int
  end
end
