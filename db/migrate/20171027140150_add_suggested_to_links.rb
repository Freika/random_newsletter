class AddSuggestedToLinks < ActiveRecord::Migration[5.1]
  def change
    add_column :links, :suggested, :boolean
    add_reference :links, :newsletter, foreign_key: true
  end
end
