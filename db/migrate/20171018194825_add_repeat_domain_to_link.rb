class AddRepeatDomainToLink < ActiveRecord::Migration[5.1]
  def change
    add_column :links, :repeat_domain, :boolean, default: false, null: false
  end
end
