user = User.create(email: 'test@test.com', password: '00000000')
p "User created: test@test.com:00000000"

newsletter = Newsletter.create!(
  user: user,
  title: "First user's newsletter",
  slug: 'first-user-newsletter'
)
p "Newsletter #{newsletter.title}"

30.times do |n|
  newsletter.issues.create!(
    title: "Issue ##{n}",
    slug: "issue-#{n}",
    number: n,
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia amet impedit adipisci doloremque iusto et odio assumenda, accusamus ratione, dolorum? Nesciunt autem voluptatibus magnam, odit voluptate sunt aliquam sequi repellendus.'
  )
  p "Issue ##{n} created"
end

3.times do |n|
  Subscriber.create(
    newsletter: newsletter,
    email: "sub#{n}@test.com",
    name: "Sub #{n}"
  )
end
