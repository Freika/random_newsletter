source 'https://rubygems.org'
ruby '2.4.2'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.4'

gem 'bootstrap', '~> 4.0.0.beta2'
gem 'coffee-rails', '~> 4.2'
gem 'devise'
gem 'draper'
gem 'figaro'
gem 'friendly_id'
gem 'jquery-rails'
gem 'kaminari'
gem 'sassc-rails', '~> 1.3'
gem 'sidekiq'
gem 'sidekiq-scheduler'
gem 'slim-rails'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'font-awesome-rails'

group :development, :test do
  gem 'rspec-rails'
  gem 'bullet'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara'
  gem 'chromedriver-helper'
  gem 'factory_girl_rails'
  gem 'ffaker'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
  gem 'shoulda-matchers'
  gem 'database_cleaner'
  gem 'awesome_print'
end

group :development do
  gem 'brakeman', require: false
  gem 'bundler-audit'
  gem 'letter_opener'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'overcommit'
  gem 'pry-rails'
  gem 'pry-doc'
  gem 'rubocop'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem 'guard-livereload', '~> 2.5', '>= 2.5.2'
end

group :test do
  gem 'capybara-screenshot'
  gem 'rails-controller-testing'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'therubyracer', platforms: :ruby
