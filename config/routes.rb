require 'sidekiq/web'
require 'sidekiq-scheduler/web'

Rails.application.routes.draw do
  mount Sidekiq::Web, at: '/sidekiq'

  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }

  namespace :app do
    resource :newsletter, only: %i[edit show update]
    resources :issues 
    namespace :issues do
      post 'send/:id', to: 'sending#create', as: 'send'
    end
  end

  resources :newsletters, only: %i[index show] do
    resources :issues, only: :show
    resources :subscribers, only: :create
    resources :links, only: :create
  end

  resources :links, only: :create

  root to: 'static_pages#home'
end
