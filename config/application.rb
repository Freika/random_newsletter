require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module RandomNewsletter
  class Application < Rails::Application
    config.load_defaults 5.1

    config.generators do |g|
      g.template_engine :slim
      g.view_specs      false
      g.helper_specs    false
      g.route_specs    false
      g.scaffold_stylesheet false
      g.fixture_replacement :factory_girl
      g.helper false
      g.assets false
      g.view_specs false
      g.request_specs false
    end

    config.active_job.queue_adapter = :sidekiq
  end
end
