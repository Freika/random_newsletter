module ApplicationHelper
  def render_side_menu?
    controller_name.eql?('issues') && action_name.eql?('show')
  end
end
