class CurrentTimeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value
      record.errors[attribute] << (options[:message] || 'time is not valid') if value < Time.current
    end
  end
end