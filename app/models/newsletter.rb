class Newsletter < ApplicationRecord
  extend FriendlyId

  belongs_to :user
  has_many :issues
  has_many :subscribers
  has_many :links

  validates :slug, presence: true, format: {
    with: /\A[a-z\-0-9\_]+\z/, message: 'invalid format'
  }, uniqueness: true

  friendly_id :slug, use: :slugged
end
