class Subscriber < ApplicationRecord
  belongs_to :newsletter

  validates :name, presence: true
  validates :email, email: true
end
