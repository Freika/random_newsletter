class Link < ApplicationRecord
  belongs_to :newsletter
  validates :url, presence: true

  def mark_repeat_domain!
    update(repeat_domain: true) if repeat_domain?
  end

  private

  def repeat_domain?
    domain = URI.parse(url).host
    Link.where('url like?', "%#{domain}%").any?
  end
end
