class Issue < ApplicationRecord
  extend FriendlyId

  belongs_to :newsletter

  validates :content, presence: true
  validates :title, presence: true, uniqueness: { scope: :newsletter_id }
  validates :send_at, current_time: true
  validates :slug, presence: true, format: {
    with: /\A[a-z\-0-9\_]+\z/, message: 'invalid format'
  }, uniqueness: { scope: :newsletter_id }

  friendly_id :slug, use: :slugged

  def perform
    set_number
    set_title
    set_slug
  end

  def set_number
    self[:number] =
      if newsletter.issues.count.positive?
        # We don't use .last here, because
        # current_user.newsletter.issues.build instance is last,
        # but we need last created in the database instance.
        newsletter.issues.order(created_at: :desc).limit(1).last.number + 1
      else
        1
      end
  end

  def set_title
    self[:title] = "#{newsletter.title} — Issue ##{number}"
  end

  def set_slug
    self[:slug] = "#{self[:title].parameterize}-#{SecureRandom.hex(3)}"
  end
end
