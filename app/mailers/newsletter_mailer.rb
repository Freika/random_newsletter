class NewsletterMailer < ApplicationMailer
  default from: 'random@example.com'

  def new_subscriber(subscriber)
    @user = subscriber.newsletter.user
    @newsletter = subscriber.newsletter
    mail(to: @user.email, subject: 'You have a new subscriber!')
  end
end
