class IssueMailer < ApplicationMailer
  default from: 'random@example.com'

  def send_issue(subscriber, issue)
    @issue_content = issue[:content]
    mail to: subscriber.email, subject: issue.newsletter[:title]
  end
end
