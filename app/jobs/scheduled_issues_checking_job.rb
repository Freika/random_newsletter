require 'sidekiq-scheduler'

class ScheduledIssuesCheckingJob < ApplicationJob
  queue_as :default

  def perform
    Issue.find_each do |issue|
      if issue[:send_at]
        if issue[:send_at] <= Time.current && issue[:sent_at].nil?
          IssueSendingJob.perform_later(issue[:id])
          issue.touch(:sent_at)
        end
      end
    end
  end
end
