class SubscriberSendingJob < ApplicationJob
  queue_as :default

  def perform(subscriber_id, issue_id)
    subscriber = Subscriber.find(subscriber_id)
    issue = Issue.find(issue_id)
    IssueMailer.send_issue(subscriber, issue).deliver_now
    issue.touch(:sent_at)
  end
end
