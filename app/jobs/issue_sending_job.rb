class IssueSendingJob < ApplicationJob
  queue_as :default

  def perform(issue_id)
    Issue.find(issue_id).newsletter.subscribers.find_each do |subscriber|
      SubscriberSendingJob.perform_later(subscriber.id, issue_id)
    end
  end
end
