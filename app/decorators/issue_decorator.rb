class IssueDecorator < ApplicationDecorator
  def display_number
    object[:number] ? "##{object[:number]}" : ''
  end
end
