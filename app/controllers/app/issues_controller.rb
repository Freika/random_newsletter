module App
  class IssuesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_newsletter, only: %i[show edit update destroy]
    before_action :set_issue, only: %i[show edit update destroy]
    def show; end

    def new
      @issue = Issue.new
    end

    def edit; end

    def create
      @issue = current_user.newsletter.issues.build(issue_params)
      @issue.perform
      if @issue.save
        redirect_to [:app, @issue], success: 'Issue was successfully created.'
      else
        render :new
      end
    end

    def update
      if @issue.update(issue_params)
        redirect_to [:app, @issue], success: 'Issue was successfully updated.'
      else
        render :edit
      end
    end

    def destroy
      @issue.destroy
      redirect_to app_newsletter_url, success: 'Issue was successfully destroyed.'
    end

    private

    def set_newsletter
      @newsletter = current_user.newsletter
    end

    def set_issue
      @issue = @newsletter.issues.friendly.find(params[:id])
    end
    
    def issue_params
      params.require(:issue).permit(:content, :send_at)
    end
  end
end
