class App::Issues::SendingController < ApplicationController
  before_action :set_current_issue, only: :create
  
  def create
    if @issue[:sent_at]
      redirect_to app_newsletter_url, warning: "#{@issue[:title]} has already been sent."
    else
      IssueSendingJob.perform_later(@issue[:id])
      redirect_to app_newsletter_url, success: "#{@issue[:title]} was succesfully sended."
    end
  end

  private

  def set_current_issue
    @issue = current_user.newsletter.issues.friendly.find(params[:id])
  end
end
 