module App
  class NewslettersController < ApplicationController
    before_action :authenticate_user!
    before_action :set_newsletter, only: %i[edit update]

    def show
      @newsletter = Newsletter.includes(:issues).find_by(user: current_user)
      @issues     = @newsletter.issues.page(params[:page]).per(20).decorate
    end

    def edit; end

    def update
      if @newsletter.update(newsletter_params)
        redirect_to app_newsletter_path, success: 'Newsletter updated.'
      else
        render :edit
      end
    end

    private

    def set_newsletter
      @newsletter = current_user.newsletter
    end

    def newsletter_params
      params.require(:newsletter).permit(:title, :description, :welcome_text, :slug)
    end
  end
end
