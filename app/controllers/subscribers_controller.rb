class SubscribersController < ApplicationController
  def create
    @newsletter = Newsletter.friendly.find(params[:newsletter_id])
    @subscriber = @newsletter.subscribers.build(subscriber_params)

    if @subscriber.save
      NewsletterMailer.new_subscriber(@subscriber).deliver_later

      redirect_to @newsletter,
        success: "You successfully subscribed to \"#{@newsletter.title}\"."
    else
      errors = @subscriber.errors.full_messages.join(', ')
      redirect_to @newsletter,
        danger: "Subscription failed! #{errors}."
    end
  end

  private

  def subscriber_params
    params.require(:subscriber).permit(:name, :email)
  end
end
