class IssuesController < ApplicationController
  def show
    @issue = Issue.friendly.find(params[:id])
  end
end
