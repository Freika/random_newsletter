class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  add_flash_types(
    :primary, :secondary, :success, :danger, :warning, :info, :light, :dark
  )
end
