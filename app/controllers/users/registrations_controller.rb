class Users::RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super

    Newsletter.create(
      user_id: resource.id, title: "#{resource.email}'s newsletter",
      slug: "newsletter-#{resource.id}"
    )
  end

  def update
    super
  end
end
