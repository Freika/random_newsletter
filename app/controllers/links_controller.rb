class LinksController < ApplicationController
  def create
    @newsletter = Newsletter.friendly.find(params[:newsletter_id])
    @link       = @newsletter.links.build(link_params)
    check_if_suggested

    if @link.save
      redirect_to @newsletter,
        success: "You successfully offer link to \"#{@newsletter.title}\"."
    else
      errors = @link.errors.full_messages.join(', ')
      redirect_to @newsletter,
        danger: "Offering link failed! #{errors}."
    end
  end


  private

  def link_params
    params.require(:link).permit(:url, :title, :description)
  end

  def check_if_suggested
    if @newsletter.user == current_user
      @link.suggested = false
    else
      @link.suggested = true
    end
  end
end
