class NewslettersController < ApplicationController
  def index
    @newsletters = Newsletter.all
  end

  def show
    @newsletter = Newsletter.friendly.find(params[:id])
    @issues     = @newsletter.issues.page(params[:page]).per(20).decorate
    @subscriber = @newsletter.subscribers.build
    @link       = @newsletter.links.build
  end
end
